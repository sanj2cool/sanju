<?php

//$db = new SQLite3('temp_db');

require __DIR__ . '/vendor/autoload.php';

use Automattic\WooCommerce\Client;

$woocommerce = new Client(
    //'https://megasellers.sell.systems/WoocommerceTST', 
    //'ck_c149094aa75028190bbb82dbbc1f80ccfb36b5dd', 
    //'cs_730e737aabb9caa2ef2787927a4bdd8845582b1f',
    'http://localhost/hotelbooking',
    'ck_e48235819421c9eed3b34b848bc9e5d65b24de71',
    'cs_afb0064fa2a80a233597839be261cda1d29ddb79',   
    [
        'wp_api' => true,
        'version' => 'wc/v3',
    ]
);

define('API_KEY','1dd917c99c05d371c6628176e5e535cf');
echo "<pre>";
print_r($woocommerce->get('products'));exit;
echo "</pre>";

////---!!!!!!--for test
//delete all categories from woocommerce
/*deleteAllCategories($woocommerce);
exit;*/










//............
////setup found index for now later if it was deleted on source site
/*
 * 
 * TEMP TABLES CATEGORIES
 *  //1 = NOT FOUND
	//0 = FOUND
	//-1 new, just inserted
 * 
 * */
$query="UPDATE categories SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE items SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE category_list SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE characteristic_list SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE facet_list SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE photo_list SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE package_list SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE price_list SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE stock_list SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............
$query="UPDATE attribute_list SET STATUS_IF_NOT_FOUND_1=1;";
$db->exec($query);
//............




//............ADD ALL CATEGORIES INTO OUR DATABASE..................//
// "application/json" example
$curl = curl_init('https://api.samsonopt.ru/v1/category/?api_key='.API_KEY);
$arHeaderList = array();
$arHeaderList[] = 'Accept: application/json';
$arHeaderList[] = 'User-Agent: string';
curl_setopt($curl, CURLOPT_HTTPHEADER, $arHeaderList);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
$result = curl_exec($curl);
curl_close($curl);
$result=json_decode($result);
//print_r($result);exit;
echo "first we need to insert all categories into our temp_db for future update on woocommerce\n";
foreach($result->data as $category){
	addNewCategorySQLLite($db,
						$category->id,
						$category->name,
						$category->parent_id,
						$category->depth_level);
}
//............
//............


//............ADD ALL ITEMS INTO OUR DATABASE..................//
// "application/json" example
$curl = curl_init('https://api.samsonopt.ru/v1/assortment/?api_key='.API_KEY);
$arHeaderList = array();
$arHeaderList[] = 'Accept: application/json';
$arHeaderList[] = 'User-Agent: string';
curl_setopt($curl, CURLOPT_HTTPHEADER, $arHeaderList);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
$result = curl_exec($curl);
curl_close($curl);
$result=json_decode($result);
//print_r($result);exit;
echo "now all ITEMS into our temp_db\n";
foreach($result->data as $item){
	addNewITEMSQLLite($db,
						$item->sku,
						$item->name,
						//array of categories for link with items in table of items_to_categories
						$item->category_list,
						$item->manufacturer,
						$item->vendor_code,
						$item->barcode,
						$item->brand,
						$item->description,
						$item->description_ext,
						$item->weight,
						$item->volume,
						//array of characteristic for link with items in table of characteristic_list
						$item->characteristic_list,
						//array of facet for link with items in table of facet_list
						$item->facet_list,
						//array of photo for link with items in table of photo_list
						$item->photo_list,
						//array of package for link with items in table of package_list
						$item->package_list,
						//array of prices for link with items in table of price_list
						$item->price_list,
						//array of stocks for link with items in table of stock_list
						$item->stock_list,
						//array of attributes for link with items in table of attribute_list
						$item->attribute_list,						
						$item->sale_date
						);
}
//............
//............







//---------------------------------UPDATE FROM TEMP DB To a Site
//get all current depth_levels
 $query="SELECT depth_level FROM CATEGORIES GROUP BY depth_level";
 $results=$db->query($query);
 $row = array();
 while($row = $results->fetchArray(SQLITE3_ASSOC)){
	if(!isset($row['depth_level']))continue;
	$depth_level=$row['depth_level'];
	updateCategoriesFromTMPtoWOO($db,$woocommerce,$depth_level);
 }
//==============================================================
//---update all items on WooCommerce
 
 //$query="SELECT * FROM items WHERE (STATUS_IF_NOT_FOUND_1=0 OR STATUS_IF_NOT_FOUND_1=-1)";
 
 $query="SELECT * FROM items 
LEFT JOIN category_list ON items.sku=category_list.item_id 
WHERE category_list.id NOT IN (SELECT id FROM categories WHERE categories.idWoocommerce<>-1) 
AND items.STATUS_IF_NOT_FOUND_1!=2 
GROUP BY sku";

 $results=$db->query($query);
 $row = array();
 while($row = $results->fetchArray(SQLITE3_ASSOC)){
	if(!isset($row['sku']))continue;
	$sku=$row['sku'];
	$itemName=$row['name'];
	$manufacturer=$row['manufacturer'];
	$vendor_code=$row['vendor_code'];
	$barcode=$row['barcode'];
	$brand=$row['brand'];
	$description=$row['description'];
	$description_ext=$row['description_ext'];
	$weight=$row['weight'];
	$volume=$row['volume'];
	$sale_date=$row['sale_date'];
	
	echo "ITEM=".$itemName."\n".
		"sku=".$sku."\n".
		"weight=".$weight."\n";
		
	//!!don't use it => there are similar titles items ///$itemID=isItemExist($woocommerce,$itemName);
	$itemID=-1;
	if($itemID==-1){
		$idWoocommerce_item=createNewItemWOO($db,$woocommerce,$sku,$itemName,$manufacturer,$vendor_code,$barcode,$brand,$description,$description_ext,$weight,$volume,$sale_date);
		$query="UPDATE items SET idWoocommerce=".$db->escapeString($idWoocommerce_item).", STATUS_IF_NOT_FOUND_1=2 WHERE sku=".$db->escapeString($sku);
		$db->exec($query);
		///update products attributes
		updateItemsDETAILSWOO($db,$woocommerce,$sku,$idWoocommerce_item);
	}else{
		$itemID=updateExistedItemWOO($db,$woocommerce,$itemID,$sku,$itemName,$manufacturer,$vendor_code,$barcode,$brand,$description,$description_ext,$weight,$volume,$sale_date);
		if($itemID>-1){
			///update products attributes
			updateItemsDETAILSWOO($db,$woocommerce,$sku,$itemID);
		}
	}
	
 }







////////.............................Additional functions....................../////////////////
////////.............................
////////.............................
//////sqlite db
//-------------items
function addNewITEMSQLLite($db,
						$sku,
						$name,
						//array of categories for link with items in table of items_to_categories
						$category_list,
						$manufacturer,
						$vendor_code,
						$barcode,
						$brand,
						$description,
						$description_ext,
						$weight,
						$volume,
						//array of characteristic for link with items in table of characteristic_list
						$characteristic_list,
						//array of facet for link with items in table of facet_list
						$facet_list,
						//array of photo for link with items in table of photo_list
						$photo_list,
						//array of package for link with items in table of package_list
						$package_list,
						//array of prices for link with items in table of price_list
						$price_list,
						//array of stocks for link with items in table of stock_list
						$stock_list,
						//array of attributes for link with items in table of attribute_list
						$attribute_list,						
						$sale_date
						){
	$foundIndex=-1;
	//check if category already exist
	$query="SELECT sku FROM items WHERE sku =".$db->escapeString($sku);
	$results=$db->query($query);
	while($row = $results->fetchArray()){
		$foundIndex=$row[0];
	}
	if($foundIndex==-1){
		$query="INSERT INTO ".
				"items(sku, name, manufacturer, vendor_code, barcode, brand, description, ".
				"description_ext, weight, volume, sale_date) ".
				"VALUES(".
					$db->escapeString($sku).",".
					"'".$db->escapeString($name)."'".",".
					"'".$db->escapeString($manufacturer)."'".",".
					"'".$db->escapeString($vendor_code)."'".",".
					"'".$db->escapeString($barcode)."'".",".
					"'".$db->escapeString($brand)."'".",".
					"'".$db->escapeString($description)."'".",".
					"'".$db->escapeString($description_ext)."'".",".
					"'".$db->escapeString($weight)."'".",".
					"'".$db->escapeString($volume)."'".",".
					"'".$db->escapeString($sale_date)."'".
					")";
		$db->exec($query);
		$foundIndex=$sku;
	}else{
		///update for show it was found
		//1 = NOT FOUND
		//0 = FOUND
		$query="UPDATE items ".
				"SET ".
				"STATUS_IF_NOT_FOUND_1=0, ".
				"name='".$db->escapeString($name)."'".",".
				"manufacturer='".$db->escapeString($manufacturer)."'".",".
				"vendor_code='".$db->escapeString($vendor_code)."'".",".
				"barcode='".$db->escapeString($barcode)."'".",".
				"brand='".$db->escapeString($brand)."'".",".
				"description='".$db->escapeString($description)."'".",".
				"description_ext='".$db->escapeString($description_ext)."'".",".
				"weight='".$db->escapeString($weight)."'".",".
				"volume='".$db->escapeString($volume)."'".",".
				"sale_date='".$db->escapeString($sale_date)."' ".
				"WHERE sku=".$db->escapeString($foundIndex);
		$db->exec($query);
	}
	
	if(!empty($category_list)){
		foreach($category_list as $category){
			updateListWITHOUTTypes($db,"category_list",$foundIndex,$category);
		}
	}	
	if(!empty($characteristic_list)){
		foreach($characteristic_list as $characteristic){
			updateListWITHOUTTypes($db,"characteristic_list",$foundIndex,$characteristic);
		}
	}
	if(!empty($facet_list)){
		foreach($facet_list as $facet){
			updateListWithTypes($db,"facet_list",$foundIndex,$facet->name,$facet->value);
		}
	}
	if(!empty($photo_list)){
		foreach($photo_list as $photo){
			updateListWITHOUTTypes($db,"photo_list",$foundIndex,$photo);
		}
	}
	if(!empty($package_list)){
		foreach($package_list as $package){
			updateListWithTypes($db,"package_list",$foundIndex,$package->type,$package->value);
		}
	}
	if(!empty($price_list)){
		foreach($price_list as $price){
			updateListWithTypes($db,"price_list",$foundIndex,$price->type,$price->value);			
		}
	}
	if(!empty($stock_list)){
		foreach($stock_list as $stock){
			updateListWithTypes($db,"stock_list",$foundIndex,$stock->type,$stock->value);
		}
	}
	if(!empty($attribute_list)){
		foreach($attribute_list as $attribute){
			updateListWithTypes($db,"attribute_list",$foundIndex,$attribute->type,$attribute->value);
		}
	}
}
//////sqlite db
//-------------updateListWithTypes
function updateListWithTypes($db,$tableName,$item_id,$type,$value){
	$foundIndex=-1;
	//check if category already exist
	$query="SELECT id FROM ".$db->escapeString($tableName).
			" WHERE item_id = ".$db->escapeString($item_id).
			" AND type ='".$db->escapeString($type)."'";
	$results=$db->query($query);
	while($row = $results->fetchArray()){
		$foundIndex=$row[0];
	}
	if($foundIndex==-1){
		$query="INSERT INTO ".$db->escapeString($tableName)."(item_id,type,value) ".
				"VALUES(".
					$db->escapeString($item_id).",".
					"'".$db->escapeString($type)."'".",".
					"'".$db->escapeString($value)."'".
				")";
		$db->exec($query);
	}else{
		///update
		//1 = NOT FOUND
		//0 = FOUND
		$query="UPDATE ".$db->escapeString($tableName).
				" SET ".
					"item_id=".$db->escapeString($item_id).",".
					"type="."'".$db->escapeString($type)."'".",".
					"value="."'".$db->escapeString($value)."',".
					"STATUS_IF_NOT_FOUND_1=0 ".
					"WHERE id=".$db->escapeString($foundIndex);
				")";
		$db->exec($query);
	}
}
//-------------updateListWITHOUTTypes
function updateListWITHOUTTypes($db,$tableName,$item_id,$value){
	$foundIndex=-1;
	//check if category already exist
	$query="SELECT id FROM ".$db->escapeString($tableName).
			" WHERE item_id = ".$db->escapeString($item_id).
			" AND value ='".$db->escapeString($value)."'";
	$results=$db->query($query);
	while($row = $results->fetchArray()){
		$foundIndex=$row[0];
	}
	if($foundIndex==-1){
		$query="INSERT INTO ".$db->escapeString($tableName)."(item_id,value) ".
				"VALUES(".
					$db->escapeString($item_id).",".
					"'".$db->escapeString($value)."'".
				")";
		$db->exec($query);
	}else{
		///update
		//1 = NOT FOUND
		//0 = FOUND
		$query="UPDATE ".$db->escapeString($tableName).
				" SET ".
					"item_id=".$db->escapeString($item_id).",".
					"value="."'".$db->escapeString($value)."',".
					"STATUS_IF_NOT_FOUND_1=0 ".
					"WHERE id=".$db->escapeString($foundIndex);
				")";
		$db->exec($query);
	}
}
//////sqlite db
//-------------categories
function addNewCategorySQLLite($db,$id,$name,$parent_id,$depth_level){
	$foundIndex=-1;
	//check if category already exist
	$query="SELECT id FROM categories WHERE id ='".$db->escapeString($id)."'";
	$results=$db->query($query);
	while($row = $results->fetchArray()){
		$foundIndex=$row[0];
	}
	if($foundIndex==-1){
		$query="INSERT INTO ".
				"categories(id,name,parent_id,depth_level) ".
				"VALUES(".
					$db->escapeString($id).",".
					"'".$db->escapeString($name)."'".",".
					$db->escapeString($parent_id)."".",".
					$db->escapeString($depth_level).
				")";
		$db->exec($query);
	}else{
		///update for show it was found
		//1 = NOT FOUND
		//0 = FOUND
		//-1 new, just inserted
		//2 updated on WOO
		$query="UPDATE categories SET STATUS_IF_NOT_FOUND_1=0 WHERE id=".$db->escapeString($foundIndex);
		$db->exec($query);
	}
}


function deleteAllCategories($woocommerce){
	$pageNumber=1;
	$iii=1;
	while($iiii=1){
		$data = [
			'page' => $pageNumber
		];
		$resultsCategories = $woocommerce->get('products/categories',$data);
		foreach($resultsCategories as $categories){
				if($categories->name!="Uncategorized"){
					$woocommerce->delete('products/categories/'.$categories->id, ['force' => true]);
					echo "Deleted category id = ".$categories->id;
				}
		}
		$pageNumber++;
	}
}

function isCategoryExist($woocommerce,$nameOfCat){
	$categoryID="-1";
	$pageNumber=1;
	$iii=1;
	while($iiii=1){
		$data = [
			'page' => $pageNumber
		];
		$resultsCategories = $woocommerce->get('products/categories',$data);
		if (empty($resultsCategories)) {
			 break;
		}
		foreach($resultsCategories as $categories){
			if($categories->name=="Uncategorized"){
				$categoryID="-1";
			}
			if($nameOfCat==$categories->name){
				$categoryID=$categories->id;
				break;
			}
		}
		$pageNumber++;
	}
	return $categoryID;
}

///find id from our warehouse 
//by slug and if found then use it for update with parent categories
function updateWithParentCategories($categoryID){
	if($categoryID!=-1){
		$data = [
			'parent' => $categoryID
		];
		$resultsCategories = $woocommerce->put('products/categories/'.$categoryID, $data);
	}
}

function createNewCategory($db,$sku,$woocommerce,$nameOfCat,$parentID=-1){
	echo "try to insert new category=".$nameOfCat."\n".
		"parentID=".$parentID."\n".
		"sku=".$sku."\n";
	$categoryDescription=$nameOfCat;
	if($parentID==-1){
		$data = [
			'name' => $nameOfCat,
			//'image' => ['src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg']
			'description' => $categoryDescription
		];
	}else{
		$data = [
			'name' => $nameOfCat,
			'parent' => $parentID,
			//'image' => ['src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg']
			'description' => $categoryDescription
		];
	}
	$resultsCategories = $woocommerce->post('products/categories', $data);
	$query="UPDATE categories SET idWoocommerce=".$db->escapeString($resultsCategories->id)." WHERE id=".$db->escapeString($sku);
	$db->exec($query);
	//1 = NOT FOUND
	//0 = FOUND
	//-1 new, just inserted
	//2 updated on WOO
	$query="UPDATE categories SET STATUS_IF_NOT_FOUND_1=2 WHERE id=".$db->escapeString($sku);
	$db->exec($query);
}

function updateExistedCategory($db,$sku,$woocommerce,$catID,$nameOfCat,$parentID=-1){
	echo "try to update existed category=".$nameOfCat."\n".
		"parentID=".$parentID."\n".
		"sku=".$sku."\n";
	$categoryDescription=$nameOfCat;
	if($parentID==-1){
		$data = [
			'name' => $nameOfCat,
			//'image' => ['src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg']
			'description' => $categoryDescription
		];
	}else{
		$data = [
			'name' => $nameOfCat,
			'parent' => $parentID,
			//'image' => ['src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg']
			'description' => $categoryDescription
		];
	}
	$resultsCategories = $woocommerce->put('products/categories/'.$catID, $data);
	$query="UPDATE categories SET idWoocommerce=".$db->escapeString($resultsCategories->id)." WHERE id=".$db->escapeString($sku);
	$db->exec($query);
	//1 = NOT FOUND
	//0 = FOUND
	//-1 new, just inserted
	//2 updated on WOO
	$query="UPDATE categories SET STATUS_IF_NOT_FOUND_1=2 WHERE id=".$db->escapeString($sku);
	$db->exec($query);
}

////////.............................
////////.............................
////////.............................

function updateCategoriesFromTMPtoWOO($db,$woocommerce,$depth_level){
 $query="SELECT * FROM CATEGORIES WHERE depth_level=".$db->escapeString($depth_level).
		" AND idWoocommerce=-1";//that means only new categories
 $results=$db->query($query);
 $row = array();
 while($row = $results->fetchArray(SQLITE3_ASSOC)){
	if(empty($row['name']))continue;
	$sku=$row['id'];
	$parent_sku=$row['parent_id'];
	$STATUS_IF_NOT_FOUND_1=$row['STATUS_IF_NOT_FOUND_1'];
	$idWoocommerce=$row['idWoocommerce'];
	if($depth_level>1){
		//get current parent id
		$queryParentID="SELECT idWoocommerce FROM CATEGORIES WHERE idWoocommerce<>-1 AND id=".$db->escapeString($parent_sku);
		$resultsParentID=$db->query($queryParentID);
		$rowParentID = array();
		$parent_idWoocommerce=-1;
		while($rowParentID = $resultsParentID->fetchArray(SQLITE3_ASSOC)){
			$parent_idWoocommerce=$rowParentID['idWoocommerce'];
			//die("sku=".$sku."; parent_sku =".$parent_sku." \n parent_idWoocommerce=".$parent_idWoocommerce);
		}
	}
	
	if($depth_level==1){
		$catID=isCategoryExist($woocommerce,$row['name']);///this is VERY VERY VERY slow 
		if(!isset($parent_idWoocommerce)){
			if($catID==-1){
				createNewCategory($db,$sku,$woocommerce,$row['name']);
			}else{
				updateExistedCategory($db,$sku,$woocommerce,$catID,$row['name']);
			}
		}else{
			if($catID==-1){
				createNewCategory($db,$sku,$woocommerce,$row['name'],$parent_idWoocommerce);
			}else{
				updateExistedCategory($db,$sku,$woocommerce,$catID,$row['name'],$parent_idWoocommerce);
			}
		}
	}
	if($depth_level>1){
		if(($parent_idWoocommerce==-1)OR($parent_idWoocommerce=="NULL")OR(empty($parent_idWoocommerce))){
			createNewCategory($db,$sku,$woocommerce,$row['name']);
		}else{
			//die("on update category parent_idWoocommerce=".$parent_idWoocommerce);
			updateExistedCategory($db,$sku,$woocommerce,$parent_idWoocommerce,$row['name']);
		}
	}
 }
}



function isItemExist($woocommerce,$title){
	$pageNumber=1;
	$iii=1;
	while($iiii=1){
		$data = [
			'page' => $pageNumber
		];
		$resultsItems = $woocommerce->get('products',$data);
		if(empty($resultsItems)){
			 break;
		}
		foreach($resultsItems as $items){
			if($title==$items->name){
				$itemsID=$items->id;
				echo(" \n \n \n ALREADY EXIST $itemsID ".$itemURL." \n \n \n ");		
				break;
			}
		}
		$pageNumber++;
	}	
	return $itemsID;
}



function updateExistedItemWOO($db,$woocommerce,$itemID,$sku,$itemName,$manufacturer,$vendor_code,$barcode,$brand,$description,$description_ext,$weight,$volume,$sale_date){
	if($itemName==''){
			return -1;
		}else{
		if(strlen($description)>30){
			$shortDescr=$description_ext.substr($description, 0, 50);
		}else{
			$shortDescr=$description_ext.$description;
		}

		$data = [
			'name' => $itemName,
			'type' => 'simple',
			'description' => $description,
			'short_description' => $description_ext." \n ".$itemName,
			'weight' => $weight,
			
		];
		$resultsCategories = $woocommerce->put('products/'.$itemID, $data);
		return $resultsCategories->id;		
	}
}

function createNewItemWOO($db,$woocommerce,$sku,$itemName,$manufacturer,$vendor_code,$barcode,$brand,$description,$description_ext,$weight,$volume,$sale_date){
	if($itemName==''){
			return -1;
	}else{
		if(strlen($description)>30){
			$shortDescr=$description_ext.substr($description, 0, 50);
		}else{
			$shortDescr=$description_ext.$description;
		}

		$data = [
			'name' => $itemName,
			'type' => 'simple',
			'description' => $description,
			'short_description' => $description_ext." \n ".$itemName,
			'weight' => $weight,
			
		];
		$resultsCategories = $woocommerce->post('products', $data);
		return $resultsCategories->id;		
	}
}


///update products attributes		
function updateItemsDETAILSWOO($db,$woocommerce,$sku,$idWoocommerce_item){
	
	//link all items with categories
	$query="SELECT * FROM category_list ".
		"LEFT JOIN categories ON categories.id=category_list.value ".
		"WHERE item_id=".$db->escapeString($sku);
	$row = array();
	$categoriesList=[];
	$results=$db->query($query);
	while($row = $results->fetchArray(SQLITE3_ASSOC)){
		if(!isset($row['idWoocommerce']))continue;
		$categoriesList[]=['id' => $row['idWoocommerce']];
	}
	//............
	
	//update photo for all items
	$query="SELECT * FROM photo_list WHERE item_id=".$db->escapeString($sku);
	$imagesList=array();
	$results=$db->query($query);
	while($row = $results->fetchArray(SQLITE3_ASSOC)){
		if(!isset($row['value']))continue;
		$imagesList[]=['src' => $row['value']];
	}
	//............
	
	//////
	$query="SELECT * FROM price_list WHERE item_id=".$db->escapeString($sku);
	$results=$db->query($query);
	while($row = $results->fetchArray(SQLITE3_ASSOC)){
		if(!isset($row['type']))continue;
		if($row['type']=="infiltration")$regular_price=$row['value'];
		if($row['type']=="contract")$sale_price=$row['value'];
	}	
	//............
	
	
	//////-------------ATTRIBUTES----------------------
	$query="SELECT * FROM characteristic_list WHERE item_id=".$db->escapeString($sku);
	$row = array();
	$attributes=[];
	$results=$db->query($query);
	while($row = $results->fetchArray(SQLITE3_ASSOC)){
		if(!isset($row['value']))continue;
		$attributes[]=[
					'name' => 'параметры', 
					'visible' => true, 
					'options' => array($row['value'])
					];
	}
	//////
	$query="SELECT * FROM facet_list WHERE item_id=".$db->escapeString($sku);	
	$row = array();
	$results=$db->query($query);
	while($row = $results->fetchArray(SQLITE3_ASSOC)){
		if(!isset($row['value']))continue;
		$attributes[]=[
					'name' => $row['type'], 
					'visible' => true, 
					'options' => array($row['value'])
					];
	}
	//////
	$query="SELECT * FROM attribute_list WHERE item_id=".$db->escapeString($sku);
	$row = array();
	$results=$db->query($query);
	while($row = $results->fetchArray(SQLITE3_ASSOC)){
		if(!isset($row['value']))continue;
		$attributes[]=[
					'name' => $row['type'], 
					'visible' => true, 
					'options' => array($row['value'])
					];
	}
	//............
	//////
	$query="SELECT * FROM package_list WHERE item_id=".$db->escapeString($sku);
	$row = array();
	$results=$db->query($query);
	while($row = $results->fetchArray(SQLITE3_ASSOC)){
		if(!isset($row['value']))continue;
		$attributes[]=[
					'name' => $row['type'], 
					'visible' => true, 
					'options' => array($row['value'])
					];
	}
	//............
	//////-------------END____OF____ATTRIBUTES----------------------//
	
	//////
	$stock_status="instock";
	$query="SELECT * FROM stock_list WHERE item_id=".$db->escapeString($sku);
	$row = array();
	$results=$db->query($query);
	while($row = $results->fetchArray(SQLITE3_ASSOC)){
		if(!isset($row['value']))continue;
		if($row['type']=="idp"){
			$stock_quantity=$row['value'];
		}
	}
	if($stock_quantity==0){$stock_quantity=1;$stock_status="outofstock";}
	//............
	
	$data = [
				'regular_price' => $regular_price,
				'stock_status' => $stock_status,
				'stock_quantity' => $stock_quantity,				
				'sale_price' => $sale_price,
				'categories' => $categoriesList,
				'images' => $imagesList,
				'attributes' => $attributes
			];
	//print_r($data);
	//print_r($woocommerce->put('products/'.$idWoocommerce_item, $data));
}
?>
